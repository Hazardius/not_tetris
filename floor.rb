# frozen_string_literal: true

##
# Default size of a floor.
DEFAULT_FLOOR_SIZE = 10

##
# This class represents a floor space on which we're dropping the bricks.
class Floor
  ##
  # Creates a new floor with given floor_size.
  #
  # Default floor_size is DEFAULT_FLOOR_SIZE.
  #
  # floor_size must be equal min 1 unit.
  def initialize(floor_size = DEFAULT_FLOOR_SIZE)
    @floor_size = floor_size
    raise ArgumentError, 'Floor space must have positive size' if @floor_size <= 0

    @floor_space = Array.new(@floor_size) { 0 }
  end

  class << self
    ##
    # Returns Regex for correct brick format line.
    def brick_format
      Regexp.new('^\d+,\d+,\d+$')
    end

    ##
    # Returns Regex for correct floor size format line.
    def size_format
      Regexp.new('^\d*$')
    end
  end

  ##
  # Changes the @floor_space to reflect the state after a new brick is dropped.
  #
  # Bricks are defined by three integer values:
  # * left  : the position of the brick's leftmost edge
  # * right : the position of the brick's rightmost edge
  # * height: the brick's height
  #
  # Both left and right needs to be a part of a (0..@floor_size) range.
  # Left cannot be bigger than right.
  def drop_brick(left, right, height)
    if left.negative? || left > @floor_size || right.negative? || right > @floor_size
      raise ArgumentError, 'Brick\'s edges must be placed on a floor space'
    elsif left > right
      raise ArgumentError,
        'Leftmost edge of a brick cannot be more on the right than its Rightmost edge'
    end

    considered_range = left..right
    # Find max height on range:
    max_height = @floor_space[considered_range].max
    # Update floor_space to reflect state after current brick was dropped:
    @floor_space.collect!.with_index do |x, i|
      considered_range.include?(i) ? max_height + height : x
    end
    # Return current highest points on a floor:
    @floor_space
  end

  ##
  # Returns the height of the highest tower all dropped bricks formed on the floor.
  def highest_point
    @floor_space.max
  end
end
