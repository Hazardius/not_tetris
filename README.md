
# not_tetris

## Requirements

To run this program you need to have ruby installed.
It was tested on ruby 2.5.1 .

## Usage

This program can be used in two modes: interactive and reading scenario from file.

### Interactive mode

If the file has permission to be executed:

    ./not_tetris.rb

In other case:

    ruby not_tetris.rb

### Read scenario from file

If we want to read scenario from a nts file:

    ./not_tetris.rb ./path/to/scanerio.nts

For an example:

    ./not_tetris.rb ./scenarios/1.nts

More informations on how to create correct scenario files can be found in [nts.format file](scenarios/nts.format).

## Attributions

Repository icon is a cropped version of photography made by [Adrian van Leen](http://taluda.openphoto.net/gallery/)
for [openphoto.net](http://openphoto.net/gallery/image/view/20365).
This image is free to use in any personal, educational, or commercial project, and has been licensed
under the following terms: [Attribution-ShareAlike](http://creativecommons.org/licenses/by-sa/3.0/).
