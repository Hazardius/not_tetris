#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative 'floor'
require_relative 'input_processing'

raise ArgumentError, 'not_tetris works only with 0 or 1 arguments' unless (0..1).cover?(ARGV.size)

if ARGV.size == 1
  # Load test from file
  file_input_mode(ARGV[0])
elsif ARGV.empty?
  # Load test from standard input
  interactive_mode
end
