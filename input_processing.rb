# frozen_string_literal: true

##
# Error message shown if floor_size string is in wrong format
FLOOR_FORMAT_ERROR = "Last input line wasn't in a format \"{int}\' or empty"
##
# Error message shown if brick string is in wrong format
BRICK_FORMAT_ERROR = "Last input line wasn't in a format \"{int},{int},{int}\'"

##
# Function for handling file input.
#
# Requires a string pointing to the scenario file(*.nts).
#
# Description of a correct *.nts file used by this program is available
# in ./scenarios/nts.format file.
def file_input_mode(path)
  require 'pathname'
  fpn = Pathname.new(path)
  raise ArgumentError, "Scenario file #{fpn} doesn't exist" unless fpn.exist?

  puts "Starting the scenario from file #{fpn}.."
  main_file_input_loop(fpn)
end

##
# Main function for handling standard input.
def interactive_mode
  print 'Please, enter the size of a floor in the example: '
  scenario_floor = Floor.new(gets.chomp.to_i)
  puts 'Enter the bricks in format(left,right,height) one row at a time.'
  puts 'Finish dropping the new bricks with an empty line.'
  main_interactive_loop(scenario_floor)
  puts "The highest tower formed with dropped bricks in a room is #{scenario_floor.highest_point} units."
end

##
# Main loop for processing interactive input of dropped bricks.
def main_interactive_loop(floor)
  next_line = gets.chomp
  while next_line != ''
    process_brick(floor, next_line)
    next_line = gets.chomp
  end
end

##
# Function processing file input.
def main_file_input_loop(pathname)
  scenario_floor = nil
  File.open(pathname, 'r').each_with_index do |line, index|
    line = line.strip
    if index.zero?
      # First line - size of the floor or empty if we want to use default.
      raise ArgumentError, FLOOR_FORMAT_ERROR if (Floor.size_format =~ line).nil?

      scenario_floor = line == '' ? Floor.new : Floor.new(line.to_i)
    elsif line != ''
      process_brick(scenario_floor, line)
    end
  end
  puts "The highest tower formed with dropped bricks in a room is #{scenario_floor.highest_point} units."
end

##
# Common part of processing a brick line.
def process_brick(floor, brick_str)
  raise ArgumentError, BRICK_FORMAT_ERROR if (Floor.brick_format =~ brick_str).nil?

  (left, right, height) = brick_str.split(',').map(&:to_i)
  floor.drop_brick(left, right, height)
end
